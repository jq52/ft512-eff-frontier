#include <cmath>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <iomanip>
#include <Eigen/Core>
#include <Eigen/Dense>
using namespace Eigen;
using namespace std;

bool is_float(string s){
  int point_num = 0;
  //int neg_num = 0;
  //cout<<s<<endl;
  if(s.size()==0) return false;
  for(unsigned int i=0;i<s.size();i++){
    //cout<<s[i]<<" ";
    if((s[i]!='.')&&((s[i]!='-')||(s[i]=='-'&&i!=0))&&(!isdigit(s[i]))){
      return false;
    }
    if(s[i]=='.') point_num++;
    //if(s[i]=='-') neg_num++;
  }
  if(point_num >1) return false;
  //if(neg_num >1) return false;
  return true;
}


double non_res_eff(unsigned int num, MatrixXd mat_A, MatrixXd vec_b,vector<vector<double> > corr_mat){
  MatrixXd weight_lam;
  weight_lam = mat_A.fullPivLu().solve(vec_b);
  double eff_vol=0;
  //cout<<"num: "<<num<<endl;
  for(unsigned int i=0;i<num;i++){
    for(unsigned int j=0;j<num;j++){
      // eff_vol+= (weight_lam(i,0) * weight_lam(j,0) * corr_mat[i][j] * vol_list[i] * vol_list[j]);
      eff_vol+= (weight_lam(i,0) * weight_lam(j,0)*mat_A(i,j));
    }
  }
  //cout<<weight_lam<<endl;
  //cout<<"a Maqt: \n"<<setprecision(4)<<mat_A<<endl;
  return sqrt(eff_vol);

}

void RemoveRow(MatrixXd& mat, unsigned int num){
  unsigned int row_num = mat.rows()-1;
  unsigned int col_num = mat.cols();
  if( num< row_num){
    mat.block(num,0,row_num-num,col_num) = mat.block(num+1,0,row_num-num,col_num);
  }
  mat.conservativeResize(row_num,col_num);
}
void RemoveColumn(MatrixXd& mat, unsigned int num){
  unsigned int row_num = mat.rows();
  unsigned int col_num = mat.cols()-1;
  if(num< col_num){
    mat.block(0,num,row_num,col_num-num) =  mat.block(0,num+1,row_num,col_num-num);
  }
  mat.conservativeResize(row_num,col_num);
}


double res_eff(unsigned int num, MatrixXd mat_A, MatrixXd vec_b,vector<vector<double> > corr_mat){
  MatrixXd weight_lam;
  weight_lam = mat_A.fullPivLu().solve(vec_b);
  double eff_vol=0;
  //cout<<"num: "<<num<<endl;
  vector<unsigned int> neg_num;
  for(unsigned int i=0;i<num;i++){
    if(weight_lam(i,0)<0) neg_num.push_back(i);
  }
  //每次删除行列会导致行列的顺序在原基础上减1，因此neg_num[i]-i消除该影响
  while(neg_num.size()!=0){
    for(unsigned int i=0;i<neg_num.size();i++){
      RemoveRow(mat_A,neg_num[i]-i);
      RemoveColumn(mat_A,neg_num[i]-i);
      RemoveRow(vec_b,neg_num[i]-i);
      // cout<<neg_num[i]<< " "<<endl;
    }
    //cout<<"mat_A:\n"<<mat_A<<endl;
    weight_lam = mat_A.fullPivLu().solve(vec_b);
    //cout<<"weight:\n"<<weight_lam<<endl;
    neg_num.clear();
    //neg_num.swap(vector());
    // neg_num.swap(vector<double>());
    // 最后两个参数的正负号不影响计算
    for(unsigned int i=0;i<weight_lam.rows()-2;i++){
      if(weight_lam(i,0)<0) neg_num.push_back(i);
    }
  }
  for(unsigned int i=0;i<weight_lam.rows()-2;i++){
    for(unsigned int j=0;j<weight_lam.rows()-2;j++){
      eff_vol+= (weight_lam(i,0) * weight_lam(j,0)*mat_A(i,j));
    }
  }

  return sqrt(eff_vol);

}

/*
double res_eff(unsigned int num, MatrixXd mat_A, MatrixXd vec_b,vector<vector<double> > corr_mat){
  MatrixXd weight_lam;
  weight_lam = mat_A.fullPivLu().solve(vec_b);
  double eff_vol=0;
  //cout<<"num: "<<num<<endl;
  vector<unsigned int> neg_num;
  for(unsigned int i=0;i<num;i++){
    if(weight_lam(i,0)<0) neg_num.push_back(i);
  }
  double min_wei = weight_lam(0,0);
  unsigned int min_ind;
  for(unsigned int i=0;i<num;i++){
    if(weight_lam(i,0)<min_wei){
      min_ind = i;
      min_wei = weight_lam(i,0);
    }
  }
  
  //每次删除行列会导致行列的顺序在原基础上减1，因此neg_num[i]-i消除该影响
  while(neg_num.size()!=0){
    
    RemoveRow(mat_A,min_ind);
    RemoveColumn(mat_A,min_ind);
    RemoveRow(vec_b,min_ind);
    num--;
    //cout<<"mat_A:\n"<<mat_A<<endl;
    weight_lam = mat_A.fullPivLu().solve(vec_b);
    //cout<<"weight:\n"<<weight_lam<<endl;
    neg_num.clear();
    min_wei = weight_lam(0,0);
    //neg_num.swap(vector());
    // neg_num.swap(vector<double>());
    // 最后两个参数的正负号不影响计算
    for(unsigned int i=0;i<weight_lam.rows()-2;i++){
      if(weight_lam(i,0)<0) neg_num.push_back(i);
    }
    for(unsigned int i=0;i<num;i++){
      if(weight_lam(i,0)<min_wei){
	min_ind = i;
	min_wei = weight_lam(i,0);
      }
    }
  }
  for(unsigned int i=0;i<weight_lam.rows()-2;i++){
    for(unsigned int j=0;j<weight_lam.rows()-2;j++){
      eff_vol+= (weight_lam(i,0) * weight_lam(j,0)*mat_A(i,j));
    }
  }

  return sqrt(eff_vol);

}
*/

/*
//直接增加一个w_i=0的限制条件
//该方法不正确
double res_eff(double exp_ret,unsigned int num, MatrixXd mat_A, MatrixXd vec_b,vector<vector<double> > corr_mat,vector<double> ret_list, vector<double> vol_list){
  MatrixXd weight_lam;
  weight_lam = mat_A.fullPivLu().solve(vec_b);
  vector<unsigned int> neg_num;
  for(unsigned int i=0;i<num;i++){
    if(weight_lam(i,0)<0) neg_num.push_back(i);
  }
  while(neg_num.size()!=0){
    unsigned int mat_A_line = mat_A.rows();
    mat_A.conservativeResize(mat_A.rows()+neg_num.size(),mat_A.cols());
    for(unsigned int i=0;i<neg_num.size();i++){
      //mat_A.conservativeResize(mat_A.rows()+neg_num.size(),mat_A.cols());
      for(unsigned int j=0;j<num;j++){
	if(j == neg_num[i]){mat_A(mat_A_line+i,j)=1;}
	else{mat_A(mat_A_line+i,j)=0;}
      }
    }
    
    cout<<"a Maqt: \n"<<mat_A<<endl;
    
    unsigned int vec_b_line = vec_b.rows();
    vec_b.conservativeResize(vec_b.rows()+neg_num.size(),vec_b.cols());
    for(unsigned int i=0;i<neg_num.size();i++){
      vec_b(vec_b_line+i,0) = 0;
    }

    cout<<"b vec: \n"<<vec_b<<endl;
    
    weight_lam = mat_A.fullPivLu().solve(vec_b);
    cout<<"weight:\n"<<weight_lam<<endl;
    
    neg_num.clear();
    //neg_num.swap(vector());
    // neg_num.swap(vector<double>());
    for(unsigned int i=0;i<num;i++){
      if(weight_lam(i,0)<0) neg_num.push_back(i);
    }
  }

  cout<<weight_lam<<endl;
  double eff_vol=0;
  //cout<<"num: "<<num<<endl;
  for(unsigned int i=0;i<num;i++){
    for(unsigned int j=0;j<num;j++){
      eff_vol+= (weight_lam(i,0) * weight_lam(j,0) * corr_mat[i][j] * vol_list[i] * vol_list[j]);
    }
  }
  return sqrt(eff_vol);

}

*/

int main(int argc, char **argv){
  if(argc!= 3 && argc!=4){
    std::cerr << "Error in argument number!\n";
    exit(EXIT_FAILURE);
  }

  const char * univ = argv[1];
  ifstream input(univ);
  if (!input.is_open()) {
    std::cerr << "Can not open the file 1.\n";
    exit(EXIT_FAILURE);
  }
  //异常处理
  if(input.peek() == EOF){
    cerr << "NULL file.\n";
    exit(EXIT_FAILURE);
  }

  std::string line;
  vector<vector<string> > univ_list;
  while(getline(input,line)){
    stringstream sstr(line);
    std::string temp;
    vector<string> line_str;
    while(getline(sstr,temp,',')){
      line_str.push_back(temp);
    }
    univ_list.push_back(line_str);
  }

  // univ_list异常处理
  for(unsigned int i=0;i<univ_list.size();i++){
    if(univ_list[i].size()!=3){
      cerr << "Every asset should have return and vol!\n";
      exit(EXIT_FAILURE);
    }
  }
  for(unsigned int i=0;i<univ_list.size();i++){
    for(unsigned int j=1;j<univ_list[i].size();j++){
      if(is_float(univ_list[i][j])==false){
	cerr<<"Invalid format in universe!\n";
	exit(EXIT_FAILURE);
      }
    }
  }
  
  const char * corr = argv[2];
  ifstream input2(corr);
  if (!input2.is_open()) {
    std::cerr << "Can not open the file 2.\n";
    exit(EXIT_FAILURE);
  }
  //异常处理                                                                                              
  if(input2.peek() == EOF){
    cerr << "NULL file.\n";
    exit(EXIT_FAILURE);
  }

  vector<vector<double> > corr_mat;
  while(getline(input2,line)){
    stringstream sstr(line);
    std::string temp;
    vector<double> line_dbl;
    while(getline(sstr,temp,',')){
      double temp_dbl;
      stringstream ss;
      // 异常处理，是否是纯数字
      if(is_float(temp)==false){
	cerr << "Invalid corr format!\n";
	exit(EXIT_FAILURE);
      }
      ss << temp;
      ss >> temp_dbl;
      line_dbl.push_back(temp_dbl);
    }
    corr_mat.push_back(line_dbl);
  }

  //corr_mat 异常处理
  if(univ_list.size()!=corr_mat.size()){
    cerr << "Corr should have the same line number as univ_list.\n";
    exit(EXIT_FAILURE);
  }
  for(unsigned int i=0;i<corr_mat.size();i++){
    if(corr_mat[i].size()!=corr_mat.size()){
      cerr << "It shouble be a N * N matrix!\n";
      exit(EXIT_FAILURE);
    }
  }

  
  
  vector<double> ret_list;
  for(unsigned int i=0;i<univ_list.size();i++){
    double temp_dbl;
    string temp;
    stringstream ss;
    temp = univ_list[i][1];
    ss << temp;
    ss >> temp_dbl;
    ret_list.push_back(temp_dbl);
  }
  vector<double> vol_list;
  for(unsigned int i=0;i<univ_list.size();i++){
    double temp_dbl;
    string temp;
    stringstream ss;
    temp = univ_list[i][2];
    ss << temp;
    ss >> temp_dbl;
    vol_list.push_back(temp_dbl);
  }
  
  /*
  for(unsigned int i=0; i<univ_list.size(); i++){
    for(unsigned int j=0;j<univ_list[i].size();j++){
      cout<<univ_list[i][j]<<" ";
    }
    cout<<"\n";
  }
  for(unsigned int i=0; i<corr_mat.size(); i++){
    for(unsigned int j=0;j<corr_mat[i].size();j++){
      cout<<corr_mat[i][j]<<" ";
    }
    cout<<"\n";
  }
  
  cout<<"return:"<<endl;
  for(unsigned int i=0; i<vol_list.size(); i++){
    cout<<ret_list[i]<<" ";
  }
  cout<<endl<<"vol:"<<endl;
  for(unsigned int i=0; i<vol_list.size(); i++){
    cout<<vol_list[i]<<" ";
  }
  cout<<endl;
  */
  unsigned int num = corr_mat[0].size();
  MatrixXd mat_A(num+2,num+2);
  for(unsigned int i=0;i<num;i++){
    for(unsigned int j=0;j<num;j++){
      mat_A(i,j)=corr_mat[i][j]*vol_list[i]*vol_list[j];
    }
  }
  for(unsigned int i=0;i<num;i++){
    mat_A(num,i)=ret_list[i];
    mat_A(i,num)=ret_list[i];
    mat_A(num+1,i)=1;
    mat_A(i,num+1)=1;
  }
  mat_A(num,num)=0;
  mat_A(num+1,num)=0;
  mat_A(num,num+1)=0;
  mat_A(num+1,num+1)=0;
  //cout<<mat_A<<endl;
  /*
  VectorXd vec_b(num+1);
  for(unsigned int i=0;i<num;i++){
    vec_b(i)=0;
  }
  */
  MatrixXd vec_b(num+2,1);
  double exp_ret = 0.03;
  //cout<<"ror: "<<exp_ret<<endl;
  for(unsigned int i=0;i<num;i++){
    vec_b(i,0)=0;
  }
  vec_b(num,0) = exp_ret;
  vec_b(num+1,0) = 1;
  // cout<< vec_b<<endl;

  //  MatrixXd weight_lam;
  // weight_lam = mat_A.fullPivLu().solve(vec_b);
  //cout<<weight_lam<<endl;
  /*
  double total_w = 0;
  for(unsigned int i=0;i<num;i++){
    total_w += weight_lam(i,0);
  }
  cout<<"total weight :"<<total_w<<endl;
  */
  
  double eff_vol=0;
  //cout<<"num: "<<num<<endl;
  /*
  for(unsigned int i=0;i<num;i++){
    for(unsigned int j=0;j<num;j++){
      eff_vol+= (weight_lam(i,0) * weight_lam(j,0) * corr_mat[i][j] * vol_list[i] * vol_list[j]);
    }
  }
  cout<<"vol: "<<sqrt(eff_vol)<<endl;
  */
  if(argc == 3){
    cout<<"ROR,volatility\n";;
    for(double i=0.01;i<.27;i=i+0.01){
      cout<<setiosflags(ios::fixed)<<setprecision(1)<<i*100.0<<"%,";
      vec_b(num,0)=i;
      eff_vol = non_res_eff(num,mat_A,vec_b,corr_mat);
      cout<<setprecision(2)<<eff_vol*100.0<<"%\n";
    }
  }
  else if(argc == 4 && strcmp(argv[3],"-r")==0){
    cout<<"ROR,volatility\n";
  //vec_b(num,0)=.14;
  //eff_vol=  res_eff(num,mat_A,vec_b,corr_mat);
  //cout<<"vol: "<<eff_vol<<endl;
  //cout<<"short:\n";
  //vec_b(num,0)=.14;
  //eff_vol=  non_res_eff(0.14,num,mat_A,vec_b,corr_mat,ret_list,vol_list);
    for(double i=0.01;i<.27;i=i+0.01){
      cout<<setiosflags(ios::fixed)<<setprecision(1)<<i*100.0<<"%,";
      vec_b(num,0)=i;
      eff_vol = res_eff(num,mat_A,vec_b,corr_mat);
      cout<<setprecision(2)<<eff_vol*100.0<<"%\n";
    }
  }
  else{
    std::cerr << "The third argument should be \"-r\".\n";
    exit(EXIT_FAILURE);
  }
}
